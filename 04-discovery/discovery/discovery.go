/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package discovery

import (
	"context"
	"github.com/coreos/etcd/mvcc/mvccpb"
	"go.etcd.io/etcd/clientv3"
	"sync"
)

type DiscoveryService struct {
	client *clientv3.Client
	list  map[string]string
	lock  sync.Mutex
}

func NewDiscoveryService(client *clientv3.Client) *DiscoveryService {
	return &DiscoveryService{
		client: client,
		list: make(map[string]string),
	}
}

func (s *DiscoveryService) Watch(ctx context.Context,prefix string) error {
	resp,err :=s.client.Get(
		ctx,
		prefix,
		clientv3.WithPrefix())
	if err!=nil{
		return err
	}

	for _, kv := range resp.Kvs {
		s.Add(string(kv.Key),string(kv.Value))
	}

	go func() {
		ch :=s.client.Watch(
			ctx,
			prefix,
			clientv3.WithPrefix())
		for resp := range ch {
			for _, event := range resp.Events {
				switch event.Type {
				case mvccpb.PUT:
					s.Add(string(event.Kv.Key),string(event.Kv.Value))
				case mvccpb.DELETE:
					s.Delete(string(event.Kv.Key))
				}
			}
		}
	}()

	return err
}

// Add return add service.
func (s *DiscoveryService) Add(k,v string)  {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.list[k] = v
}

// Delete return removes the specified service from the service list.
func (s *DiscoveryService) Delete(key string) {
	s.lock.Lock()
	defer s.lock.Unlock()
	delete(s.list, key)
}

// Get return service list.
func (s *DiscoveryService) Get() []string  {
	s.lock.Lock()
	defer s.lock.Unlock()

	list := make([]string,0,len(s.list))
	for _, s2 := range s.list {
		list = append(list, s2)
	}
	return list
}

func (s *DiscoveryService) Close() error  {
	return s.client.Close()
}





