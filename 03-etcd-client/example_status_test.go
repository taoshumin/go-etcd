/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"io/ioutil"
	"net/http"
	"testing"
)

/*
	获取etcd状态
 */
func Test_Etcd_Status(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	resp, err := client.Status(context.Background(), "localhost:2379")
	if err!=nil{
		t.Error(err)
	}

	t.Logf("endpoint:  Leader: %v", resp.Header.MemberId == resp.Leader)
}

/*
	碎片整理
 */
func Test_Etcd_Defragment(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	resp,err :=client.Defragment(context.Background(),"localhost:2379")
	if err!=nil{
		t.Error(err)
	}
	t.Log("Defragment :",resp)
}

/*
	监控指标
 */
func Test_Etcd_Metrics(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	url := "http://" + "localhost:2379" + "/metrics"
	resp, err := http.Get(url)
	if err!=nil{
		t.Error(err)
	}
	defer 	resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err!=nil{
		t.Error(err)
	}

	t.Log("metrics: ",string(b))
}