/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/pkg/transport"
	"log"
	"testing"
	"time"
)

var (
	dailTimeout = 5 * time.Second
	endpoints   = []string{"localhost:2379"}
)

func DefaultNewClient() *clientv3.Client {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: dailTimeout,
	})
	if err != nil {
		log.Fatal(err)
	}
	return client
}

func DefaultUserRoleNewClient(username,password string) *clientv3.Client {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: dailTimeout,
		Username: username,
		Password: password,
	})
	if err!=nil{
		log.Fatal(err)
	}
	return client
}

func Test_New(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	_, err := client.Put(context.Background(), "foo", "bar")
	if err != nil {
		t.Error(err)
	}
}

func Test_New_TLS(t *testing.T) {
	tlsInfo := transport.TLSInfo{
		CertFile:      "/tmp/test-certs/test-name-1.pem",
		KeyFile:       "/tmp/test-certs/test-name-1-key.pem",
		TrustedCAFile: "/tmp/test-certs/trusted-ca.pem",
	}

	tlsConfig, err := tlsInfo.ClientConfig()
	if err != nil {
		t.Error(err)
	}

	client, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: dailTimeout,
		TLS:         tlsConfig,
	})
	if err != nil {
		t.Error(err)
	}

	defer client.Close()

	_, err = client.Put(context.Background(), "foo", "bar")
	if err != nil {
		t.Error(err)
	}
}
