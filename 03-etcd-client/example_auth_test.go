/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"testing"
)

/*
	# 创建完整的root权限
	etcdctl role add root
	etcdctl user add root
	etcdctl user grant-role root root

	# 完整的授权流程
	etcdctl role add role0
	etcdctl role grant-permission role0 readwrite foo
	etcdctl user add user0
	etcdctl user grant-role user0 role0
	-------------
	# 开启权限
	etcdctl auth enable
	etcdctl --user=user0:1 put foo bar
	etcdctl get foo
 */

// 注意：必须先创建root角色和root用户
func Test_Etcd_Auth(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	// 创建root角色
	if _, err := client.RoleAdd(context.Background(), "root"); err != nil {
		t.Error(err)
	}

	// 创建root用户
	if _, err := client.UserAdd(context.Background(), "root", "123"); err != nil {
		t.Error(err)
	}

	// 授权root角色给root用户
	if _, err := client.UserGrantRole(context.Background(), "root", "root"); err != nil {
		t.Error(err)
	}

	// 创建root0角色
	if _,err :=client.RoleAdd(context.Background(),"root0");err !=nil{
		t.Error(err)
	}

	// 创建user0用户
	if _,err :=client.UserAdd(context.Background(),"user0","123");err !=nil{
		t.Error(err)
	}

	if _, err := client.UserGrantRole(context.Background(), "user0", "root0"); err != nil {
		t.Error(err)
	}

	// 给role0角色绑定资源权限  ( role grant-permission role0 readwrite foo)
	if _,err :=client.RoleGrantPermission(context.Background(),
		"role0",
		"foo",
		"zoo",
		clientv3.PermissionType(clientv3.PermReadWrite),
		);err !=nil{
		t.Error(err)
	}

	// 开启授权
	if _,err :=client.AuthEnable(context.Background());err !=nil{
		t.Error(err)
	}

	// 用户user0登录
	cc :=DefaultUserRoleNewClient("user0","123")

	// put
	if _,err :=cc.Put(context.Background(),"foo","bar");err !=nil{
		t.Error(err)
	}

	// get
	if _,err :=cc.Get(context.Background(),"foo");err !=nil{
		t.Error(err)
	}

	defer cc.Close()

	// 关闭授权
	if _,err :=client.AuthDisable(context.Background());err !=nil{
		t.Error(err)
	}
}

func Test_Etcd_Auth_Get(t *testing.T) {
	cc :=DefaultUserRoleNewClient("user0","123")
	if _,err :=cc.Get(context.Background(),"foo");err !=nil{
		t.Error(err)
	}
	defer cc.Close()
}
