/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"testing"
)

func Test_Lease_Grant(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	// 创建租约
	resp,err :=client.Grant(context.Background(),5)
	if err!=nil{
		t.Error(err)
	}

	_,err =client.Put(context.Background(),
		"lease-foo","bar",
		clientv3.WithLease(resp.ID)) // 获取租约id
	if err!=nil{
		t.Error(err)
	}
}

func Test_Lease_Revoke(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	resp,err :=client.Grant(context.Background(),10)
	if err!=nil{
		t.Error(err)
	}

	_,err =client.Put(context.Background(),
		"lease-foo","bar",
		clientv3.WithLease(resp.ID)) // 获取租约id
	if err!=nil{
		t.Error(err)
	}

	gresp, err := client.Get(context.TODO(), "lease-foo")
	if err!=nil{
		t.Error(err)
	}

	t.Log("key lenth:",len(gresp.Kvs))

	// 移除租约
	_, err = client.Revoke(context.TODO(), resp.ID)
	if err != nil {
		t.Error(err)
	}


	gresp, err = client.Get(context.TODO(), "lease-foo")
	if err!=nil{
		t.Error(err)
	}

	t.Log("key lenth:",len(gresp.Kvs))
}

// etcd 租约一直保持alive状态
func Test_Lease_KeepAlive(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	resp,err :=client.Grant(context.Background(),5)
	if err!=nil{
		t.Error(err)
	}

	_,err =client.Put(context.Background(),
		"lease-foo","bar",
		clientv3.WithLease(resp.ID))
	if err!=nil{
		t.Error(err)
	}

	ch, err := client.KeepAlive(context.TODO(), resp.ID)
	if err!=nil{
		t.Error(err)
	}

	ka :=<- ch

	t.Log("keepalive : ",ka)
	//select {
	//
	//}
}

func Test_Lease_KeepAlive_Once(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	resp,err :=client.Grant(context.Background(),5)
	if err!=nil{
		t.Error(err)
	}

	_,err =client.Put(context.Background(),
		"lease-foo","bar",
		clientv3.WithLease(resp.ID))
	if err!=nil{
		t.Error(err)
	}

	ka, err := client.KeepAliveOnce(context.TODO(), resp.ID)
	if err!=nil{
		t.Error(err)
	}

	t.Log("keepalive once: ",ka)
}


