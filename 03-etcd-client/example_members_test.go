/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"testing"
)

func Test_Member_List(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	list, err := client.MemberList(context.Background())
	if err != nil {
		t.Error(err)
	}

	t.Logf("member list: %d", len(list.Members))
}

func Test_Member_Add(t *testing.T) {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints[:2],
		DialTimeout: dailTimeout,
	})
	if err != nil {
		t.Error(err)
	}

	defer client.Close()

	urls := endpoints[2:]
	resp, err := client.MemberAdd(context.Background(), urls)
	if err != nil {
		t.Error(err)
	}

	t.Logf("add member urls: %s", resp.Member.PeerURLs)
}

func Test_Member_Remove(t *testing.T) {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints[1:],
		DialTimeout: dailTimeout,
	})
	if err != nil {
		t.Error(err)
	}
	defer client.Close()

	resp, err := client.MemberList(context.Background())
	if err != nil {
		t.Error(err)
	}

	_, err = client.MemberRemove(context.Background(), resp.Members[0].ID)
	if err != nil {
		t.Error(err)
	}
}

func Test_Member_Update(t *testing.T) {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: dailTimeout,
	})
	if err != nil {
		t.Error(err)
	}
	defer client.Close()

	resp, err := client.MemberList(context.Background())
	if err != nil {
		t.Error(err)
	}

	peerURLs := []string{"http://localhost:12380"}
	_, err = client.MemberUpdate(context.Background(), resp.Members[0].ID, peerURLs)
	if err != nil {
		t.Error(err)
	}
}
