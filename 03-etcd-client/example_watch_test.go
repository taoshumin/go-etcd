/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"testing"
)

func Test_Watcher_watch(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	ch := client.Watch(context.Background(), "foo")

	for c := range ch {
		for _, ev := range c.Events {
			t.Logf("%s %q : %q\n", ev.Type, ev.Kv.Key, ev.Kv.Value)
		}
	}
}

func Test_Watcher_watchWithPrefix(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	ch := client.Watch(context.Background(), "foo", clientv3.WithPrefix())
	for c := range ch {
		for _, ev := range c.Events {
			t.Logf("%s %q : %q\n", ev.Type, ev.Kv.Key, ev.Kv.Value)
		}
	}
}

// PUT "foo1" : "bar"
// PUT "foo2" : "bar"
// PUT "foo3" : "bar"

func Test_Watcher_watchWithRange(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	// watches within ['foo1', 'foo4'), in lexicographical order
	ch := client.Watch(context.Background(), "foo", clientv3.WithRange("foo4"))
	for c := range ch {
		for _, ev := range c.Events {
			t.Logf("%s %q : %q\n", ev.Type, ev.Kv.Key, ev.Kv.Value)
		}
	}
}

func Test_Watcher_watchWithProgressNotify(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	rch := client.Watch(context.Background(), "foo", clientv3.WithProgressNotify())
	for wresp := range rch {
		t.Logf("Revision: %d IsProgressNotify: %v", wresp.Header.Revision, wresp.IsProgressNotify())
	}
}
