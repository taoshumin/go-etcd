/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clientv3_test

import (
	"context"
	"fmt"
	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/etcdserver/api/v3rpc/rpctypes"
	"testing"
	"time"
)

func Test_Kv_Put(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	_,err:=client.Put(ctx,"some_key","some_value")
	if err!=nil{
		t.Error(err)
	}
}

func Test_Kv_Put_Error(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	_, err := client.Put(ctx, "", "sample_value")
	if err!=nil{
		switch err {
		case context.Canceled:
			t.Logf("ctx is canceled")
		case context.DeadlineExceeded:
			t.Logf("ctx dead line exceeded")
		case rpctypes.ErrEmptyKey:
			t.Logf("rpc empty key")
		default:
			t.Logf("bad cluster endpoints")
		}
	}
}

func Test_Kv_Get(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	if _,err :=client.Put(context.TODO(), "foo", "bar");err!=nil{
		t.Error(err)
	}

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	resp,err:=client.Get(ctx,"foo")
	if err!=nil{
		t.Error(err)
	}

	for _,ev := range resp.Kvs{
		t.Logf("%s :%s \n",ev.Key,ev.Value)
	}
}

func Test_Kv_GetRev(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	presp,err :=client.Put(context.Background(),"foo10","bar1")
	if err!=nil{
		t.Error(err)
	}

	_,err = client.Put(context.Background(),"foo10","bar2")
	if err!=nil{
		t.Error(err)
	}

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	// 获取指定存储的版本
	resp,err :=client.Get(ctx,"foo10",clientv3.WithRev(presp.Header.Revision))
	if err!=nil{
		t.Error(err)
	}

	for _, kv := range resp.Kvs {
		t.Logf("%s : %s\n",kv.Key,kv.Value)
	}

	// OutPut: foo :bar1
}

func Test_Kv_Sort_Prefix(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	for i := 0; i < 5; i++ {
		ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
		_,_ =client.Put(ctx,fmt.Sprintf("key-%d",i),fmt.Sprintf("value-%d",i))
		cancel()
	}

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	// 获取前缀含有key的键值，按照key的键排降序
	resp, err := client.Get(ctx, "key",
		clientv3.WithPrefix(),
		clientv3.WithSort(clientv3.SortByKey, clientv3.SortDescend))
	if err!=nil{
		t.Error(err)
	}

	for _, ev := range resp.Kvs {
		fmt.Printf("%s : %s\n", ev.Key, ev.Value)
	}
}

func Test_Kv_Delete(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	_, err := client.Get(ctx, "key", clientv3.WithPrefix())
	if err!=nil{
		t.Error(err)
	}

	dresp,err :=client.Delete(ctx,"key",clientv3.WithPrefix())
	if err!=nil{
		t.Error(err)
	}

	t.Logf("delete all key: %v",dresp.PrevKvs)
}

func Test_Kv_Compare(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	resp,err :=client.Get(ctx,"foo")
	if err!=nil{
		t.Error(err)
	}

	compRev := resp.Header.Revision

	ctx,cancel = context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	// 压缩 etcd 键值对存储中的事件历史
	_,err =client.Compact(ctx,compRev)
	if err!=nil{
		t.Error(err)
	}
}

// 事务
func Test_Kv_Tx(t *testing.T) {
	client :=DefaultNewClient()
	defer client.Close()

	kvc :=clientv3.NewKV(client)
	_,err :=kvc.Put(context.Background(),"key","xyz")
	if err!=nil{
		t.Error(err)
	}

	ctx,cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()

	_,err =kvc.Txn(ctx).
		If(clientv3.Compare(clientv3.Value("key"),">","abc")).
		Then(clientv3.OpPut("key","XYZ1")).
		Else(clientv3.OpPut("key","ABC1")).
		Commit()

	if err!=nil{
		t.Error(err)
	}

	gresp,err :=kvc.Get(context.Background(),"key")
	if err!=nil{
		t.Error(err)
	}

	for _, ev := range gresp.Kvs {
		fmt.Printf("%s : %s\n", ev.Key, ev.Value)
	}
}

func Test_Kv_Do(t *testing.T) {
	client := DefaultNewClient()
	defer client.Close()

	ops := []clientv3.Op{
		clientv3.OpPut("put-key", "123"),
		clientv3.OpGet("put-key"),
		clientv3.OpPut("put-key", "456")}

	for _, op := range ops {
		if _, err := client.Do(context.TODO(), op); err != nil {
			t.Error(err)
		}
	}
}