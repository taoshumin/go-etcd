### [etcd](https://etcd.io/docs/v3.4/demo/#auth)

[官方文档](https://etcd.io/docs/v3.4/demo/#auth)

##### 一、[etcdkeeper安装部署](https://developer.aliyun.com/article/720495)
##### 二、[Etcd环境安装与简介](https://gitlab.com/taoshumin/go-etcd/-/blob/main/01.%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/01.etcd-install.md)
##### 三、[Etcd使用说明](https://gitlab.com/taoshumin/go-etcd/-/blob/main/01.%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/02.etcd-use.md)
##### 四、[Etcd分布式锁](https://gitlab.com/taoshumin/go-etcd/-/tree/main/02-etcd-concurrency-lock)



### 教程 - 

* [etcd环境安装与使用](https://www.cnblogs.com/FireworksEasyCool/p/12858570.html)
* [etcd实现服务发现](https://www.cnblogs.com/FireworksEasyCool/p/12890649.html)
* [gRPC负载均衡（客户端负载均衡）--基于etcd服务发现](https://www.cnblogs.com/FireworksEasyCool/p/12912839.html)
* [gRPC负载均衡（自定义负载均衡策略）--基于etcd服务发现](https://www.cnblogs.com/FireworksEasyCool/p/12924701.html)
* [etcd分布式锁及事务](https://www.cnblogs.com/FireworksEasyCool/p/12937882.html)



